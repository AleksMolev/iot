package ru.molev.alex.iot.svc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.molev.alex.iot.model.HeartBeat;
import ru.molev.alex.iot.repo.HeartBeatRepo;
import ru.molev.alex.iot.svc.api.HeartBeatSvc;
import java.util.List;

@Service
public class HeartBeatSvcImpl implements HeartBeatSvc {

    private HeartBeatRepo heartBeatRepo;

    @Autowired
    public HeartBeatSvcImpl(HeartBeatRepo heartBeatRepo) {
        this.heartBeatRepo = heartBeatRepo;
    }

    @Override
    public List<HeartBeat> getHeartBeats() {
        return heartBeatRepo.getHeartBeats();
    }
}