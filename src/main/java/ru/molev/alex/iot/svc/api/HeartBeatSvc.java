package ru.molev.alex.iot.svc.api;

import ru.molev.alex.iot.model.HeartBeat;
import java.util.List;

public interface HeartBeatSvc {
    List<HeartBeat> getHeartBeats();
}