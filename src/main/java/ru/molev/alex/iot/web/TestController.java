package ru.molev.alex.iot.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.molev.alex.iot.svc.api.HeartBeatSvc;
import ru.molev.alex.iot.web.dto.HeartBeatDto;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private HeartBeatSvc heartBeatSvc;

    @GetMapping("/location")
    private Mono<Set<HeartBeatDto>> getHeartbeat() {
        return Mono.defer( () -> Mono.just(
                heartBeatSvc.getHeartBeats().stream().map(HeartBeatDto::new).collect(Collectors.toSet())));
    }
}