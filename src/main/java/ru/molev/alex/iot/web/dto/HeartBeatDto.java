package ru.molev.alex.iot.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.molev.alex.iot.model.HeartBeat;

@Data
@AllArgsConstructor
public class HeartBeatDto {
    private Double latitude;
    private Double longitude;
    private Integer battery;
    
    public HeartBeatDto(HeartBeat heartBeat) {
        this.latitude = heartBeat.getLatitude();
        this.longitude = heartBeat.getLongitude();
        this.battery = heartBeat.getBattery();
    }
}
