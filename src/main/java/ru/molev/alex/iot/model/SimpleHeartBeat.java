package ru.molev.alex.iot.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SimpleHeartBeat implements HeartBeat {
    private Double latitude;
    private Double longitude;
    private Integer battery;
}