package ru.molev.alex.iot.model;

public interface HeartBeat {
    Double getLatitude();
    Double getLongitude();
    Integer getBattery();
}
