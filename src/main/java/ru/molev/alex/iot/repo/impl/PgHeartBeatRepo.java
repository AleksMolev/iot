package ru.molev.alex.iot.repo.impl;

import org.jooq.DSLContext;
import org.jooq.RecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.molev.alex.iot.model.HeartBeat;
import ru.molev.alex.iot.model.SimpleHeartBeat;
import ru.molev.alex.iot.repo.HeartBeatRepo;
import java.util.List;

import static ru.molev.alex.iot.jooq.tables.Heartbeat.HEARTBEAT;
import ru.molev.alex.iot.jooq.tables.records.HeartbeatRecord;

import javax.sql.DataSource;

@Repository
public class PgHeartBeatRepo implements HeartBeatRepo {
    private DSLContext dsl;

    private RecordMapper<HeartbeatRecord, HeartBeat> heartBeatMapper = record -> 
        new SimpleHeartBeat(record.getLatitude(), record.getLongitude(), record.getBattery());

    @Autowired
    public PgHeartBeatRepo(DSLContext dsl, DataSource ds) {
        this.dsl = dsl;
    }

    @Override
    public List<HeartBeat> getHeartBeats() {
        return dsl.selectFrom(HEARTBEAT)
                .fetch(heartBeatMapper);
    }
}