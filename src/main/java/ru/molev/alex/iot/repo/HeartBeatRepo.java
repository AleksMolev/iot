package ru.molev.alex.iot.repo;

import ru.molev.alex.iot.model.HeartBeat;
import java.util.List;

public interface HeartBeatRepo {
    List<HeartBeat> getHeartBeats();
}