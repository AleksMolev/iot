CREATE TABLE config.ping_settings
(
    battery integer,
    latitude double precision,
    longitude double precision,
    id bigint NOT NULL,
    CONSTRAINT heartbeat_pkey PRIMARY KEY (id)
)