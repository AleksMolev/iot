
CREATE SCHEMA data AUTHORIZATION admin;


CREATE SCHEMA config
    AUTHORIZATION admin;

COMMENT ON SCHEMA config
    IS 'Contains different configuration of app';



CREATE TABLE data.heartbeat
(
    battery integer,
    latitude double precision,
    longitude double precision,
    id bigint NOT NULL,
    CONSTRAINT heartbeat_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE data.heartbeat
    OWNER to admin;

GRANT ALL ON TABLE data.heartbeat TO admin;


INSERT INTO data.heartbeat(
	battery, latitude, longitude, id)
	VALUES (57, 11.712341343, 1.17098898, 1),
	(98, 8.456641343, 2.21098898, 2),
	(4, 3.234241343, 2.54098898, 3);