# iot

## Compile project
mvn clean compile

## Run migration
mvn flyway:migrate

## Run docker
Go to folder resources/docker/main and execute
   docker-compose up